# role

A role template for doing one thing and doing it well.

# Usage

```bash
  $ aptivate-cli template -t role
```
