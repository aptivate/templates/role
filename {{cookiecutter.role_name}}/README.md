[![pipeline status](https://git.coop/aptivate/ansible-roles/{{ cookiecutter.role_name }}/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-roles/{{ cookiecutter.role_name }}/commits/master)

# {{ cookiecutter.role_name }}

{{ cookiecutter.description }}

# Requirements

> --- TODO: Replace this with useful information described in below text ---
> Any pre-requisites that may not be covered by Ansible itself or the role
> should be mentioned here. For instance, if the role uses the EC2 module, it
> may be a good idea to mention in this section that the boto package is
> required.

# Role Variables

> --- TODO: Replace this with useful information described in below text ---
> A description of the settable variables for this role should go here,
> including any variables that are in defaults/main.yml, vars/main.yml, and any
> variables that can/should be set via parameters to the role. Any variables
> that are read from other roles and/or the global scope (ie. hostvars, group
> vars, etc.) should be mentioned here as well.

# Dependencies

> --- TODO: Replace this with useful information described in below text ---
> A list of other roles hosted on Galaxy should go here, plus any details in
> regards to parameters that may need to be set for other roles, or variables
> that are used from other roles.

# Example Playbook

```yaml
- hosts: localhost
  roles:
     - role: {{ cookiecutter.role_name }}
```

# Testing

> --- TODO: Replace this with useful information described in below text ---
> We use molecule.readthedocs.io for testing. There are a number of drivers to
> choose from but the most common for us are: Docker and Linode. Please try to
> stick to these drivers for consistency. Once you have decided which driver
> you need, just run:
> $ pipenv run molecule init scenario -r {{ cookiecutter.role_name }} -d <driver>

```bash
$ pipenv install --dev
$ pipenv run molecule test
```

# License

  * https://www.gnu.org/licenses/gpl-3.0.en.html

# Author Information

  * https://aptivate.org/
  * https://git.coop/aptivate
